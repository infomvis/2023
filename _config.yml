# Welcome to Jekyll!
#
# This config file is meant for settings that affect your whole blog, values
# which you are expected to set up once and rarely edit after that. If you find
# yourself editing this file very often, consider using Jekyll's data files
# feature for the data you need to update frequently.
#
# For technical reasons, this file is *NOT* reloaded automatically when you use
# 'bundle exec jekyll serve'. If you change this file, please restart the server process.
#
# If you need help with YAML syntax, here are some quick references for you:
# https://learn-the-web.algonquindesign.ca/topics/markdown-yaml-cheat-sheet/#yaml
# https://learnxinyminutes.com/docs/yaml/
#
# Site settings
# These are used to personalize your new site. If you look in the HTML files,
# you will see them accessed via {{ site.title }}, {{ site.email }}, and so on.
# You can create any custom variable you would like, and they will be accessible
# in the templates via {{ site.myvariable }}.

title: Information Visualization
author: Michael Behrisch
description: >- # this means to ignore newlines until "baseurl:"
  Applied data analytics is a multidisciplinary field where you will learn insights needed to make sense of data, research, and observations from everyday life.

  You will learn how to apply a data-driven approach to problem solving, but will not only learn about tools, methods, and techniques, or the latest trends, but also more generic insights: why do certain approaches work, why the field is so popular, what common mistakes are made.

  The lectures will provide the theoretical background of how a data analytics process should be performed.

  Furthermore, we discuss an overview of popular data analytics and visualization techniques to help match techniques with information needs, including applications of text mining and data enrichment.
baseurl: "/2023" # the subpath of your site, e.g. /blog
url: "https://infomvis.gitlab.io" # the base hostname & protocol for your site, e.g. http://example.com
teamsurl: "https://teams.microsoft.com/l/team/19%3aS1UWvatMvv8O-iKn1QNKEr9ySvqA1lSrhf4KijuXoFo1%40thread.tacv2/conversations?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7" # the base hostname & protocol for your site, e.g. http://example.com
lang: en
timezone: UTC

# Social media
twitter_username: utrechtvig
twitter_image:
facebook_app_id:
facebook_image:
google_analytics:
comments: false
gitlab_username: vig

# Build settings
theme: jekyll-whiteglass
plugins:
  - jekyll-archives
  - jekyll-paginate
  - jekyll-sitemap

# Exclude from processing.
# The following items will not be processed, by default.
# Any item listed under the `exclude:` key here will be automatically added to
# the internal "default list".
#
# Excluded items can be processed by explicitly listing the directories or
# their entries' file path in the `include:` list.
#
# exclude:
#   - .sass-cache/
#   - .jekyll-cache/
#   - gemfiles/
#   - Gemfile
#   - Gemfile.lock
#   - node_modules/
#   - vendor/bundle/
#   - vendor/cache/
#   - vendor/gems/
#   - vendor/ruby/
exclude:
  - README.md

# Outputting
permalink: /:year/:month/:day/:title/

# Markdown settings
markdown: kramdown
excerpt_separator: "<!-- more -->"
kramdown:
  auto_ids: false
  enable_coderay: false
  entity_output: as_char
  footnote_nr: 1
  smart_quotes: lsquo,rsquo,ldquo,rdquo
  toc_levels: 1..6

# jekyll-archives
jekyll-archives:
  enabled:
    - categories
    - tags
  layout: category_archives
  permalinks:
    category: /categories/:name/
    tag: /tags/:name/

# jekyll-paginate
paginate_path: /posts/:num/
paginate: 5
