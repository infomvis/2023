---
layout: page
title: Resources
permalink: /resources/
order: 7
menu: Resources
---

## Textbooks

{% include_relative textbooks.md %}

## Theory Classes
- [Resources under Files on MS Teams (General)](https://teams.microsoft.com/l/channel/19%3aS1UWvatMvv8O-iKn1QNKEr9ySvqA1lSrhf4KijuXoFo1%40thread.tacv2/General?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7)

## Labs

### Final Project
- [Project Description](../assets/Project_Assignment_Sheet_2023.pdf)

### Groups
- [Group Creation Here](https://docs.google.com/spreadsheets/d/1bzN5P2qS1l22KMv8e-7OPaF-ZQdMZzak/edit?usp=sharing&ouid=112856120778314990316&rtpof=true&sd=true)

### Instructions
- [Week1](https://infomvis.gitlab.io/2023/lab-week1/)
- [Week2](https://infomvis.gitlab.io/2023/lab-week2/)
- [Week3](https://infomvis.gitlab.io/2023/lab-week3/)
- [Week4](https://infomvis.gitlab.io/2023/lab-week4/)
- [Week5](https://infomvis.gitlab.io/2023/lab-week5/)
- [Week6](https://infomvis.gitlab.io/2023/lab-week6/)
- [Week7](https://infomvis.gitlab.io/2023/lab-week7/)

### HW
- [Week1](https://infomvis.gitlab.io/2023/hw-week1/)
- [Week2](https://infomvis.gitlab.io/2023/hw-week2/)
- [Week3](https://infomvis.gitlab.io/2023/hw-week3/)
- [Week4](https://infomvis.gitlab.io/2023/hw-week4/)
- [Week5](https://infomvis.gitlab.io/2023/hw-week5/)
- [Week6](https://infomvis.gitlab.io/2023/hw-week6/)

### HW Solutions
- [Week1](https://surfdrive.surf.nl/files/index.php/s/criWWiWlLqmxATF)
- [Week2](https://surfdrive.surf.nl/files/index.php/s/kdzk1p0ibgU2wXl)
- [Week3](https://surfdrive.surf.nl/files/index.php/s/yfEu5RHR0Yhxol3)
- [Week4](https://surfdrive.surf.nl/files/index.php/s/Vy6b7KOJWKqFYSL)
- [Week5](https://surfdrive.surf.nl/files/index.php/s/L9WdeCFPrazcWjJ)
- [Week6](https://surfdrive.surf.nl/files/index.php/s/auIuGBUZOuiYy9g)
- [Week7](https://surfdrive.surf.nl/files/index.php/s/OeYlCVPGdSvl6r5)

### HW Summary Feedback
- [Week1](https://docs.google.com/document/d/1O6RzipOmShMxCovETsORpwP3oUAgQBAWSfupkkyxV-o/edit?usp=sharing)
- [Week2](https://docs.google.com/document/d/1dKR6D7qyc4f6bZ6BAnnlcxaIqSyyobTZOx7N6Cla_Ko/edit?usp=sharing)
- [Week3](https://docs.google.com/document/d/1ncfkfHHHjUnEWcMurSPJEFXBeXFCVAbE1LtR6qnkEos/edit?usp=sharing)
- [Week4](https://docs.google.com/document/d/1vUg3J_TG9vwuxoyKPzbags6Ns8vh63vgTYfIhIkRZ9A/edit?usp=sharing)
- [Week5](https://docs.google.com/document/d/1eMVA52dHUJQTqCWn4ONKLUEssbiUC0CXBbC9VPcCZZc/edit?usp=sharing)
- [Week6](https://docs.google.com/document/d/1ANXaIZcpoiWARgfTuhTJYmevMXXzQgewxacOmc3utAM/edit?usp=sharing)
- [Week7](https://docs.google.com/document/d/1kY9dvcwYVE4GOapbzk-F8XX-aCwfoGAM3oFXBXzyHaQ/edit?usp=sharing)

## D3

D3.js is a JavaScript library for manipulating documents based on data. D3 helps you bring data to life using HTML, SVG, and CSS.

[Official D3 Site](http://d3js.org/)  
[Official D3 API reference (version 4)](https://github.com/d3/d3/blob/master/API.md)  
[Official D3 API reference (version 3.x)](https://github.com/d3/d3-3.x-api-reference/blob/master/API-Reference.md)  
[D3 Tutorial by Scott Murray (D3 version 3)](http://alignedleft.com/tutorials/d3/)  
[Jerome Cukier's D3 Cheat Sheet](http://www.jeromecukier.net/wp-content/uploads/2012/10/d3-cheat-sheet.pdf)  
[D3 Observable Gallery](https://observablehq.com/@d3/gallery)

## Web Development

[Courseduck Javascript](https://courseduck.com/programming/javascript/) - Listing of best online JS courses
[WebStorm by JetBrains](https://www.jetbrains.com/webstorm/) - Smart JavaScript IDE ([free for students](https://www.jetbrains.com/student/))  
[Eloquent Javascript Book](http://eloquentjavascript.net/)  
[DevDocs](http://devdocs.io/), [Mozilla Developer Network](https://developer.mozilla.org/en-US/) - Unified documentations on major web technologies  
[Github (Git repository hosting service)](https://github.com/) - Version control system for software development ([free private repositories for students](https://education.github.com/pack))

## Tableau

[Tableau Software](http://tableau.com) is a tool that lets you create interactive data visualizations. ([free for students](http://www.tableau.com/academic/students))

## Data Sources

[Data.gov](http://www.data.gov/)  
[Census.gov](http://www.census.gov/)  
[Dataverse Network](http://thedata.org/)  
[Climate Data Sources](http://www.realclimate.org/index.php/data-sources/)  
[Climate Station Records](http://www.metoffice.gov.uk/climatechange/science/monitoring/subsets.html)  
[CDC Data (Disease Control and Prevention)](http://www.cdc.gov/nchs/data_access/data_tools.htm)  
[World Bank Catalog](http://data.worldbank.org/data-catalog)  
[Free SVG Maps](http://www.d-maps.com/index.php?lang=en)  
[UK Office for National Statistics](http://www.statistics.gov.uk/default.asp)  
[StateMaster](http://www.statemaster.com/index.php)  
[Quandl](http://www.quandl.com)

## Web Sites & Blogs

[Flowing Data](http://flowingdata.com/)  
[Visual Complexity](http://www.visualcomplexity.com/vc/)  
[Guardian DataBlog](http://www.guardian.co.uk/news/datablog)  
[The Upshot](http://www.nytimes.com/section/upshot)


