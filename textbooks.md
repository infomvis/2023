#### Course Textbooks
_for theory lectures:_ \
textbook: [Visualization Analysis and Design](https://www.amazon.com/Visualization-Analysis-Design-AK-Peters/dp/1466508914) 

_for labs:_ \
textbook: [Interactive Data Visualization for the Web: An Introduction to Designing with D3 2nd Edition](https://www.amazon.com/Interactive-Data-Visualization-Web-Introduction/dp/1491921285) also [download here](https://surfdrive.surf.nl/files/index.php/s/76BclTxcki8nbFm)



